<?php

namespace App\Controller;

use App\Entity\Tweet;
use App\Entity\User;
use App\Form\TweetType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PageController extends AbstractController
{
    /**
     * @Route("/", name="app_home", methods={"GET", "POST"})
     * @param Request $request
     * @return Response
     */
    public function homePage(Request $request): Response {

        /**
         * @var User $currentUser
         */
        $currentUser = $this->getUser();

        // On récupère l'information de la requête
        $name = $request->query->get('name'); // équivaut à $_GET['name']

        // On va chercher tous les Tweet en BDD
        $tweets = $this->getDoctrine()
            ->getRepository(Tweet::class)
            ->findAll();

        // On instancie (on créé) un nouveau Formulaire pour un nouveau Tweet
        $tweet = new Tweet();

        $form = $this->createForm(TweetType::class, $tweet);

        // On dit au form de gérer la requête HTTP
        $form->handleRequest($request);

        // On regarde si les données du form sont envoyées et valides
        if ($form->isSubmitted() && $form->isValid()) {

            /**
             * @var Tweet $tweet
             */
            $tweet = $form->getData(); // On récupère les données sous forme d'entité Tweet
            $tweet->setAuthor($currentUser); // On définit l'utilisateur connecté comme auteur du Tweet

            // On dit au manager de Doctrine d'enregistrer le nouveau Tweet
            $manager = $this->getDoctrine()->getManager();
            $manager->persist($tweet);
            $manager->flush();

            return $this->redirectToRoute('app_home');
        }

        // Le controller rend le template en injectant les paramètres
        // dont il a besoin
        return $this->render('page/index.html.twig', [
            // variable Twig => variable PHP,
            'username' => $name,
            'date' => new \DateTime(),
            'tweets' => $tweets,
            // on injecte la "vue" du formulaire
            'tweetForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/mentions-legales", name="app_legals")
     */
    public function legalsPage() {
        return $this->render("page/cgu.html.twig");
        // return new Response("Mentions légales");
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function loginPage() {
        return $this->render("page/login.html.twig");
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function registerPage() {
        return $this->render("page/register.html.twig");
    }

    /**
     * @Route("/users/profile", name="app_user_profile")
     */
    public function profilePage() {
        return $this->render("page/profile.html.twig");
    }

    /**
     * @Route("/feed", name="app_feed")
     */
    public function feedPage() {
        return $this->render("page/feed.html.twig");
    }

    /**
     * @Route("/users", name="app_users_list")
     */
    public function usersPage() {
        return $this->render("page/users.html.twig");
    }
}
