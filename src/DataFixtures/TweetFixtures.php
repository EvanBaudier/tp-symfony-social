<?php

namespace App\DataFixtures;

use App\Entity\Tweet;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Class TweetFixtures
 * @package App\DataFixtures
 *
 * On s'assure que les Fixtures des Users sont déjà chargées en base de données
 * avant de lancer les fixtures Tweet, grave à l'interface DependentFixtureInterface.
 * De cette manière, on va pouvoir associer des Users existants en tant qu'auteurs des Tweets.
 */
class TweetFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        // On récupère tous les utilisateurs existants ...
        $users = $manager->getRepository(User::class)->findAll();

        // ... et on génère une dizaine de Tweets a chacun
        foreach ($users as $user) {

            for ($i = 0; $i < rand(8, 12); $i++) {
                // On crée un nouvel objet Tweet
                $tweet = new Tweet();
                $tweet->setMessage($faker->realText(140));
                $tweet->setAuthor($user);

                // On informe Doctrine qu'un nouveau Tweet a été créé
                $manager->persist($tweet);
            }
        }

        // On ordonne a Doctrine d'enregistrer les nouvelles données
        // en base de données
        $manager->flush();
    }

    public function getDependencies()
    {
        return [UserFixtures::class];
    }
}

